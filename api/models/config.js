const mongoose = require('mongoose');
const configSchema = mongoose.Schema(
    {
        id_device: {
            type: String,
            required: [true, 'device_id is empty']
        },
        userId: {
            type: String
        },
        calendar_id: {
            type: String
        },
    }
)

module.exports = mongoose.model('Config', configSchema, 'Config');