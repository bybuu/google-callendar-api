const mongoose = require('mongoose');
const tokenSchema = mongoose.Schema(
    { 
        access_token: {
            type: String,
            required: [true, 'access_token is empty']
        },
        refresh_token: {
            type: String,
            required: [true, 'refresh_token is empty']
        },
        scope: {
            type: String,
            required: [true, 'scope is empty']
        },
        token_type: {
            type: String,
            required: [true, 'token_type is empty']
        },
        id_token: {
            type: String,
            required: [true, 'id_token is empty']
        },
        expiry_date: {
            type: Number,
            required: [true, 'Number is empty']
        },
        userId: {
            type: String,
            required: [true, 'userId is empty']
        }
    });
/* global db */
module.exports = mongoose.model('Token', tokenSchema, 'Token');