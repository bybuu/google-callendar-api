const Config = require('../models/config');
const mongoose = require('mongoose');

class ConfigService {
    constructor () {
      // Create instance of Data Access layer using our desired model
    }

    async upsert ( dataConfig ) {
        const filter = { 'id_device': dataConfig.id_device };
        let doc = await Config.findOneAndUpdate(filter, dataConfig, {
            new: true,
            upsert: true // Make this update into an upsert
        });
        return doc;
    }

    async findOne (id_device) {
        const res = await Config.findOne({ 'id_device': id_device }).exec();
        return res;
    }
}

module.exports = ConfigService;