const Token = require('../models/token');

class TokenService {
    constructor () {
      // Create instance of Data Access layer using our desired model
    
    }

    async create ( tokenToCreate ) {
        const res = await tokenToCreate.save().exec();
        return res;
    }

    async update ( tokenToUpdate ) {
        tokenToUpdate.update().then(result => {
            res.status(200).json({
                thisToken: token
            })
        })
        .catch(err => console.log(err));
    }

    async upsert ( dataToken ) {
        const filter = { 'userId': dataToken.userId };
        let doc = await Token.findOneAndUpdate(filter, dataToken, {
            new: true,
            upsert: true // Make this update into an upsert
          });
        return doc;
    }

    async findOne (userId) {
        const res = await Token.findOne({ 'userId': userId }).exec();
        return res;
    }
}

module.exports = TokenService;