const {google} = require('googleapis');
const credentials = require('../../resources/credentials.json');

class CalendarService {
    
    constructor () {
      // Create instance of Data Access layer using our desired model
      
    }

    async createCalendar (token, thisSummary) {
        const auth = this.getAuth();
        auth.setCredentials(token);
        var calendarData = {
            summary: thisSummary
        }
        const calendar = google.calendar({version: 'v3', auth});
        const res = await calendar.calendars.insert({
            resource: {
            summary: thisSummary
            }
        });
        return res.data.id;
    }

    async listEvents(token, thisCalendarId) {
        if (thisCalendarId == null) {
            thisCalendarId ='primary'
        }
        const auth = this.getAuth();
        auth.setCredentials(token);
        const calendar = google.calendar({version: 'v3', auth});
        const list = await calendar.events.list({
          calendarId: thisCalendarId,
          timeMin: (new Date()).toISOString(),
          maxResults: 10,
          singleEvents: true,
          orderBy: 'startTime',
        });
        const events = list.data.items;
        return events;
      }

      async createEvent(token, event,thisCalendarId) {
        if (thisCalendarId == null) {
            thisCalendarId ='primary'
        }
        const auth = this.getAuth();
        auth.setCredentials(token);
        const calendar = google.calendar({version: 'v3', auth});
        const eventAnswer = await calendar.events.insert({
          auth: auth,
          calendarId: thisCalendarId,
          resource: event,
          sendNotifications: true
        });
        return eventAnswer;
      }

      async getUserInfo(token) {
        const auth = this.getAuth();
        auth.setCredentials(token);
        const service = google.people({version: 'v1', auth});
        const people = await service.people.get({
            resourceName: 'people/me',
            personFields: 'names',
        });
        return people.data.resourceName;
      }

      async generateToken(code) {
        const SCOPES = ['https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/contacts.readonly', 'profile'];
        const auth = this.getAuth();
        auth.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });
        const token = await auth.getToken(code);
        return token.tokens;
      }

      getAuth() {
        const {client_secret, client_id, redirect_uris} = credentials.web;
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        return oAuth2Client;
      }

      getTokenLink() {
        const SCOPES = ['https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/contacts.readonly', 'profile'];
        const auth = this.getAuth();
        return auth.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });
      }

}
module.exports = CalendarService;