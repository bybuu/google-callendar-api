const express = require('express');
var router = express.Router();
const TokenService = require('../services/service-token');
const CalendarService = require('../services/service-google-calendar');
const ConfigService = require('../services/service-config');
const tokenService = new TokenService();
const calendarService = new CalendarService();
const configService = new ConfigService();

router
    .route('/upsertToken')
        .post(async (req, res) => {
            console.log("Upsert token");
            const token = buildGoogleToken(req.body);
            const userId = await calendarService.getUserInfo(token);
            const dataToken = buildDataToken(token,userId);
            const resToken = await tokenService.upsert(dataToken);
            res.status(200).json({
                        thisToken: resToken
            })
        })

router
    .route('/createCalendar')
        .post(async (req, res) => {
            console.log("Create calendar");
            const conf = await configService.findOne(req.query.id);
            const tokenObject = await tokenService.findOne(conf.userId);
            if (tokenObject ==null) {
                res.status(200).json({
                    thisToken: tokenObject
                })
            }
            token = buildGoogleToken(tokenObject);
            const resCalendar = await calendarService.createCalendar(token, req.body.summary);
            const dataConfig = buildDataConfig(req.query.id,conf.userId,resCalendar);
            await configService.upsert(dataConfig);
            res.status(200).json({
                            thisCalendar: resCalendar
            })
        })

router
    .route('/getEvents')
        .get(async (req, res) => {
                console.log("Get events");
                const conf = await configService.findOne(req.query.id);
                const tokenObject = await tokenService.findOne(conf.userId);
                if (tokenObject ==null) {
                    res.status(400).json({
                        thisToken: tokenObject
                    })
                }
                token = buildGoogleToken(tokenObject);
                const resCalendar = await calendarService.listEvents(token,conf.calendar_id);
                res.status(200).json({
                                thisCalendar: resCalendar
                })
        })

router
    .route('/createEvent')
        .post(async (req, res) => {
            console.log("Create event");
            const conf = await configService.findOne(req.query.id);
            const tokenObject = await tokenService.findOne(conf.userId);
            if (tokenObject ==null) {
                res.status(200).json({
                    thisToken: tokenObject
                })
            }
            token = buildGoogleToken(tokenObject);
            const resCalendar = calendarService.createEvent(token, req.body.event, conf.calendar_id);
            res.status(200).json({
                    thisCalendar: resCalendar
            })
        })

router
    .route('/createToken')
        .post(async (req, res) => {
            const newToken = await calendarService.generateToken(req.body.code);
            const token = buildGoogleToken(newToken);
            const userId = await calendarService.getUserInfo(token);
            const dataToken = buildDataToken(token,userId);
            await tokenService.upsert(dataToken);
            const dataConfig = buildDataConfig(req.query.id,userId,null);
            await configService.upsert(dataConfig);
            res.status(200).json({
                        thisToken: dataToken
            })
        })

router
    .route('/urlToken')
        .get( (req, res) => {
            console.log("Create token");
            const thisUrl = calendarService.getTokenLink();
            res.status(200).json({
                url: thisUrl
            })
        })

router
    .route('/getConfig')
        .get( async (req, res) => {
            console.log("Get config");
            const id_device = req.query.id;
            const resDevice = await configService.findOne(id_device);
            if (resDevice == null) {
                res.status(400).json({
                    device: null
                })
            }
            else {
                res.status(200).json({
                    device: resDevice
                })
            }
        })

function buildGoogleToken(object) {
    return {
        access_token: object.access_token,
        refresh_token: object.refresh_token,
        scope: object.scope,
        token_type: object.token_type,
        id_token: object.id_token,
        expiry_date: object.expiry_date
    }
}

function buildDataToken(object, currentUserId) {
    if (object.refresh_token == null) {
        return {
            access_token: object.access_token,
            scope: object.scope,
            token_type: object.token_type,
            id_token: object.id_token,
            expiry_date: object.expiry_date,
            userId: currentUserId,
        }
    }
    return {
        access_token: object.access_token,
        refresh_token: object.refresh_token,
        scope: object.scope,
        token_type: object.token_type,
        id_token: object.id_token,
        expiry_date: object.expiry_date,
        userId: currentUserId,
    }
}

function buildDataConfig(id_deviceData, userIdData, calendar_idData) {
    return {
        id_device: id_deviceData,
        userId: userIdData,
        calendar_id:calendar_idData 
    }
}
    
module.exports = router;