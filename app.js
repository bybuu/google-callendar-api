const express = require('express');
const app = express();

const calendar = require('./api/routes/calendar');

app.use(express.json())
app.use('/calendar', calendar);

var mongoose = require('mongoose');
var mongoDB = ''; //mongodbserver
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = app;