const http = require('http');
const port = process.env.port || 3000;

const app = require('./app');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
const server = http.createServer(app);

server.listen(port);